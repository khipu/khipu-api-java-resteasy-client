package com.khipu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.*;

import java.io.IOException;
import java.text.DateFormat;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.ext.ContextResolver;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen")public class JSON {
  private ObjectMapper mapper;

  public JSON() {
    mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
    mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
    mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
  }

  /**
  * Serialize the given Java object into JSON string.
  * @param obj Object to serialize
  * @return Serialized object as String
  * @throws ApiException if fails to make API call
  *
  */
  public String serialize(Object obj) throws ApiException {
      try {
          if (obj != null)
              return mapper.writeValueAsString(obj);
          else
              return null;
      } catch (Exception e) {
          throw new ApiException(400, e.getMessage());
      }
  }

  /**
   * Serialize the given Java object into canonical JSON string
   * @param obj Object to serialize
   * @return Serialized object as String
   * @throws ApiException if fails to make API call
   *
   */
  public String serializeCanonical(Object obj) throws ApiException {
      try {
          if (obj != null){
              Object tmpObject = mapper.readValue(mapper.writeValueAsString(obj), Object.class);
              return mapper.writeValueAsString(tmpObject);
          } else
              return null;
      } catch (Exception e) {
          throw new ApiException(400, e.getMessage());
      }
  }

  public JSON setDateFormat(DateFormat dateFormat) {
      mapper.setDateFormat(dateFormat);
      return this;
  }

  /**
     * Deserialize the given JSON string to Java object.
     *
     * @param body The JSON string
     * @param returnType The object to deserialize
     * @param <T> The object's type
     * @return De-serialized Java object
     * @throws ApiException if fails to make API call
     */
  public <T> T deserialize(String body, GenericType<T> returnType) throws ApiException {
      JavaType javaType = mapper.constructType(returnType.getType());
      try {
          return mapper.readValue(body, javaType);
      } catch (IOException e) {
          if (returnType.getType().equals(String.class)) {
              return (T) body;
          } else{
              throw new ApiException(500, e.getMessage(), null, body);
          }
      }
  }
}
