/*
 * khipu API
 * khipu API
 *
 * OpenAPI spec version: 2.9.1-resteasy
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.khipu.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}
