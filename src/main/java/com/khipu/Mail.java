package com.khipu;

import com.khipu.api.client.PaymentsApi;
import com.khipu.api.model.PaymentsCreateResponse;

import java.util.HashMap;
import java.util.Map;

public class Mail {
    public static void main(String[] args) throws ApiException {

        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("http://localkhipu.com:8080/payment/api/2.0");
        apiClient.setKhipuCredentials(249139, "2971dc87cf8f6d9c36f193432f4d32afbc6dd74e");
        PaymentsApi paymentsApi = new PaymentsApi();
        paymentsApi.setApiClient(apiClient);


        Map<String, Object> options = new HashMap<>();
        options.put("transactionId", "MTI-100");
        options.put("returnUrl", "http://mi-ecomerce.com/backend/return");
        options.put("cancelUrl", "http://mi-ecomerce.com/backend/cancel");
        options.put("pictureUrl", "http://mi-ecomerce.com/pictures/foto-producto.jpg");
        options.put("apiVersion", "1.3");

        PaymentsCreateResponse response = paymentsApi.paymentsPost("Compra de prueba de la API" //Motivo de la compra
                , "CLP" //Moneda
                , 100.0 //Monto
                , options
        );

        System.out.println("resp = " + paymentsApi.paymentsIdGet(response.getPaymentId()));
    }
}
