/*
 * khipu API
 * khipu API
 *
 * OpenAPI spec version: 2.9.1-resteasy
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.khipu.api.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
@JsonIgnoreProperties(ignoreUnknown = true)
/**
 * PaymentMethodItem
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen")
public class PaymentMethodItem {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("logo_url")
  private String logoUrl = null;

  public PaymentMethodItem id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Identificador del medio de pago
   * @return id
  **/
  @Schema(description = "Identificador del medio de pago")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PaymentMethodItem name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Nombre del medio de pago
   * @return name
  **/
  @Schema(description = "Nombre del medio de pago")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PaymentMethodItem logoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
    return this;
  }

   /**
   * URL del logo sugerido para mostrar
   * @return logoUrl
  **/
  @Schema(description = "URL del logo sugerido para mostrar")
  public String getLogoUrl() {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentMethodItem paymentMethodItem = (PaymentMethodItem) o;
    return Objects.equals(this.id, paymentMethodItem.id) &&
        Objects.equals(this.name, paymentMethodItem.name) &&
        Objects.equals(this.logoUrl, paymentMethodItem.logoUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, logoUrl);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentMethodItem {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    logoUrl: ").append(toIndentedString(logoUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
