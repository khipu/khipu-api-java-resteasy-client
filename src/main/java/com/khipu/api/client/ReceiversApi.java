package com.khipu.api.client;

import com.khipu.ApiException;
import com.khipu.ApiClient;
import com.khipu.Configuration;
import com.khipu.Pair;

import javax.ws.rs.core.GenericType;

import com.khipu.api.model.AuthorizationError;
import com.khipu.api.model.ReceiversCreateResponse;
import com.khipu.api.model.ServiceError;
import com.khipu.api.model.ValidationError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen")public class ReceiversApi {
  private ApiClient apiClient;

  public ReceiversApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ReceiversApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Crear una nueva cuenta de cobro
   * Crear una nueva cuenta de cobro asociada a un integrador. Necesita datos de la cuenta de usuario asociada, datos de facturación y datos de contacto.
      * @param adminFirstName  (required)
      * @param adminLastName  (required)
      * @param adminEmail  (required)
      * @param countryCode  (required)
      * @param businessIdentifier  (required)
      * @param businessCategory  (required)
      * @param businessName  (required)
      * @param businessPhone  (required)
      * @param businessAddressLine1  (required)
      * @param businessAddressLine2  (required)
      * @param businessAddressLine3  (required)
      * @param contactFullName  (required)
      * @param contactJobTitle  (required)
      * @param contactEmail  (required)
      * @param contactPhone  (required)
      
      
      
      
      
      
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
                                                * - bankAccountBankId    * - bankAccountIdentifier    * - bankAccountName    * - bankAccountNumber    * - notifyUrl    * - renditionUrl 
   * End possible optional params list
   *
      * @return ReceiversCreateResponse
   * @throws ApiException if fails to make API call
   */
  public ReceiversCreateResponse receiversPost(String adminFirstName, String adminLastName, String adminEmail, String countryCode, String businessIdentifier, String businessCategory, String businessName, String businessPhone, String businessAddressLine1, String businessAddressLine2, String businessAddressLine3, String contactFullName, String contactJobTitle, String contactEmail, String contactPhone, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'adminFirstName' is set
    if (adminFirstName == null) {
      throw new ApiException(400, "Missing the required parameter 'adminFirstName' when calling receiversPost");
    }
    // verify the required parameter 'adminLastName' is set
    if (adminLastName == null) {
      throw new ApiException(400, "Missing the required parameter 'adminLastName' when calling receiversPost");
    }
    // verify the required parameter 'adminEmail' is set
    if (adminEmail == null) {
      throw new ApiException(400, "Missing the required parameter 'adminEmail' when calling receiversPost");
    }
    // verify the required parameter 'countryCode' is set
    if (countryCode == null) {
      throw new ApiException(400, "Missing the required parameter 'countryCode' when calling receiversPost");
    }
    // verify the required parameter 'businessIdentifier' is set
    if (businessIdentifier == null) {
      throw new ApiException(400, "Missing the required parameter 'businessIdentifier' when calling receiversPost");
    }
    // verify the required parameter 'businessCategory' is set
    if (businessCategory == null) {
      throw new ApiException(400, "Missing the required parameter 'businessCategory' when calling receiversPost");
    }
    // verify the required parameter 'businessName' is set
    if (businessName == null) {
      throw new ApiException(400, "Missing the required parameter 'businessName' when calling receiversPost");
    }
    // verify the required parameter 'businessPhone' is set
    if (businessPhone == null) {
      throw new ApiException(400, "Missing the required parameter 'businessPhone' when calling receiversPost");
    }
    // verify the required parameter 'businessAddressLine1' is set
    if (businessAddressLine1 == null) {
      throw new ApiException(400, "Missing the required parameter 'businessAddressLine1' when calling receiversPost");
    }
    // verify the required parameter 'businessAddressLine2' is set
    if (businessAddressLine2 == null) {
      throw new ApiException(400, "Missing the required parameter 'businessAddressLine2' when calling receiversPost");
    }
    // verify the required parameter 'businessAddressLine3' is set
    if (businessAddressLine3 == null) {
      throw new ApiException(400, "Missing the required parameter 'businessAddressLine3' when calling receiversPost");
    }
    // verify the required parameter 'contactFullName' is set
    if (contactFullName == null) {
      throw new ApiException(400, "Missing the required parameter 'contactFullName' when calling receiversPost");
    }
    // verify the required parameter 'contactJobTitle' is set
    if (contactJobTitle == null) {
      throw new ApiException(400, "Missing the required parameter 'contactJobTitle' when calling receiversPost");
    }
    // verify the required parameter 'contactEmail' is set
    if (contactEmail == null) {
      throw new ApiException(400, "Missing the required parameter 'contactEmail' when calling receiversPost");
    }
    // verify the required parameter 'contactPhone' is set
    if (contactPhone == null) {
      throw new ApiException(400, "Missing the required parameter 'contactPhone' when calling receiversPost");
    }
    // create path and map variables
    String localVarPath = "/receivers".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }

    if (adminFirstName != null) {
        localVarFormParams.put("admin_first_name", adminFirstName);
    }
    if (adminLastName != null) {
        localVarFormParams.put("admin_last_name", adminLastName);
    }
    if (adminEmail != null) {
        localVarFormParams.put("admin_email", adminEmail);
    }
    if (countryCode != null) {
        localVarFormParams.put("country_code", countryCode);
    }
    if (businessIdentifier != null) {
        localVarFormParams.put("business_identifier", businessIdentifier);
    }
    if (businessCategory != null) {
        localVarFormParams.put("business_category", businessCategory);
    }
    if (businessName != null) {
        localVarFormParams.put("business_name", businessName);
    }
    if (businessPhone != null) {
        localVarFormParams.put("business_phone", businessPhone);
    }
    if (businessAddressLine1 != null) {
        localVarFormParams.put("business_address_line_1", businessAddressLine1);
    }
    if (businessAddressLine2 != null) {
        localVarFormParams.put("business_address_line_2", businessAddressLine2);
    }
    if (businessAddressLine3 != null) {
        localVarFormParams.put("business_address_line_3", businessAddressLine3);
    }
    if (contactFullName != null) {
        localVarFormParams.put("contact_full_name", contactFullName);
    }
    if (contactJobTitle != null) {
        localVarFormParams.put("contact_job_title", contactJobTitle);
    }
    if (contactEmail != null) {
        localVarFormParams.put("contact_email", contactEmail);
    }
    if (contactPhone != null) {
        localVarFormParams.put("contact_phone", contactPhone);
    }

    if(options != null) {
        if(options.containsKey("bankAccountBankId") && options.get("bankAccountBankId") != null) {
            localVarFormParams.put("bank_account_bank_id", options.get("bankAccountBankId"));
        }
        if(options.containsKey("bankAccountIdentifier") && options.get("bankAccountIdentifier") != null) {
            localVarFormParams.put("bank_account_identifier", options.get("bankAccountIdentifier"));
        }
        if(options.containsKey("bankAccountName") && options.get("bankAccountName") != null) {
            localVarFormParams.put("bank_account_name", options.get("bankAccountName"));
        }
        if(options.containsKey("bankAccountNumber") && options.get("bankAccountNumber") != null) {
            localVarFormParams.put("bank_account_number", options.get("bankAccountNumber"));
        }
        if(options.containsKey("notifyUrl") && options.get("notifyUrl") != null) {
            localVarFormParams.put("notify_url", options.get("notifyUrl"));
        }
        if(options.containsKey("renditionUrl") && options.get("renditionUrl") != null) {
            localVarFormParams.put("rendition_url", options.get("renditionUrl"));
        }
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/x-www-form-urlencoded"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<ReceiversCreateResponse> localVarReturnType = new GenericType<ReceiversCreateResponse>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }

  /**
  * Crear una nueva cuenta de cobro
  * Crear una nueva cuenta de cobro asociada a un integrador. Necesita datos de la cuenta de usuario asociada, datos de facturación y datos de contacto.
  * @param adminFirstName  (required)
  * @param adminLastName  (required)
  * @param adminEmail  (required)
  * @param countryCode  (required)
  * @param businessIdentifier  (required)
  * @param businessCategory  (required)
  * @param businessName  (required)
  * @param businessPhone  (required)
  * @param businessAddressLine1  (required)
  * @param businessAddressLine2  (required)
  * @param businessAddressLine3  (required)
  * @param contactFullName  (required)
  * @param contactJobTitle  (required)
  * @param contactEmail  (required)
  * @param contactPhone  (required)
  * @return ReceiversCreateResponse
  * @throws ApiException if fails to make API call
   */
  public ReceiversCreateResponse receiversPost(String adminFirstName, String adminLastName, String adminEmail, String countryCode, String businessIdentifier, String businessCategory, String businessName, String businessPhone, String businessAddressLine1, String businessAddressLine2, String businessAddressLine3, String contactFullName, String contactJobTitle, String contactEmail, String contactPhone) throws ApiException {
    return receiversPost(adminFirstName, adminLastName, adminEmail, countryCode, businessIdentifier, businessCategory, businessName, businessPhone, businessAddressLine1, businessAddressLine2, businessAddressLine3, contactFullName, contactJobTitle, contactEmail, contactPhone, null);
  
  }
}
