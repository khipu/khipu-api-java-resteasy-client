package com.khipu.api.client;

import com.khipu.ApiException;
import com.khipu.ApiClient;
import com.khipu.Configuration;
import com.khipu.Pair;

import javax.ws.rs.core.GenericType;

import com.khipu.api.model.AuthorizationError;
import com.khipu.api.model.PaymentMethodsResponse;
import com.khipu.api.model.ServiceError;
import com.khipu.api.model.ValidationError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen")public class PaymentMethodsApi {
  private ApiClient apiClient;

  public PaymentMethodsApi() {
    this(Configuration.getDefaultApiClient());
  }

  public PaymentMethodsApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Obtener listado de medios de pago disponible para una cuenta de cobrador
   * Obtiene el listado de medios de pago que pueden usarse para pagar a esta cuenta de cobro.
      * @param id Identificador del merchant (required)
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
   
   * End possible optional params list
   *
      * @return PaymentMethodsResponse
   * @throws ApiException if fails to make API call
   */
  public PaymentMethodsResponse merchantsIdPaymentMethodsGet(String id, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling merchantsIdPaymentMethodsGet");
    }
    // create path and map variables
    String localVarPath = "/merchants/{id}/paymentMethods".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }


    if(options != null) {
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<PaymentMethodsResponse> localVarReturnType = new GenericType<PaymentMethodsResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }

  /**
  * Obtener listado de medios de pago disponible para una cuenta de cobrador
  * Obtiene el listado de medios de pago que pueden usarse para pagar a esta cuenta de cobro.
  * @param id Identificador del merchant (required)
  * @return PaymentMethodsResponse
  * @throws ApiException if fails to make API call
   */
  public PaymentMethodsResponse merchantsIdPaymentMethodsGet(String id) throws ApiException {
    return merchantsIdPaymentMethodsGet(id, null);
  
  }
}
