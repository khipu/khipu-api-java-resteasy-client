package com.khipu.api.client;

import com.khipu.ApiException;
import com.khipu.ApiClient;
import com.khipu.Configuration;
import com.khipu.Pair;

import javax.ws.rs.core.GenericType;

import com.khipu.api.model.AuthorizationError;
import java.util.Date;
import com.khipu.api.model.PaymentsCreateResponse;
import com.khipu.api.model.PaymentsResponse;
import com.khipu.api.model.ServiceError;
import com.khipu.api.model.SuccessResponse;
import com.khipu.api.model.ValidationError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen")public class PaymentsApi {
  private ApiClient apiClient;

  public PaymentsApi() {
    this(Configuration.getDefaultApiClient());
  }

  public PaymentsApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Obtener información de un pago
   * Información completa del pago. Datos con los que fue creado y el estado actual del pago. Se obtiene del notification_token que envia khipu cuando el pago es conciliado.
      * @param notificationToken Token de notifiación recibido usando la API de notificaiones 1.3 o superior. (required)
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
   
   * End possible optional params list
   *
      * @return PaymentsResponse
   * @throws ApiException if fails to make API call
   */
  public PaymentsResponse paymentsGet(String notificationToken, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'notificationToken' is set
    if (notificationToken == null) {
      throw new ApiException(400, "Missing the required parameter 'notificationToken' when calling paymentsGet");
    }
    // create path and map variables
    String localVarPath = "/payments".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();

    localVarQueryParams.addAll(apiClient.parameterToPairs("", "notification_token", notificationToken));

    if (options != null) {
    }


    if(options != null) {
    }


    if(options != null) {
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<PaymentsResponse> localVarReturnType = new GenericType<PaymentsResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * Confirmar el pago.
   * Al confirmar el pago, este será rendido al día siguiente.
      * @param id Identificador del pago (required)
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
   
   * End possible optional params list
   *
      * @return SuccessResponse
   * @throws ApiException if fails to make API call
   */
  public SuccessResponse paymentsIdConfirmPost(String id, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling paymentsIdConfirmPost");
    }
    // create path and map variables
    String localVarPath = "/payments/{id}/confirm".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }


    if(options != null) {
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<SuccessResponse> localVarReturnType = new GenericType<SuccessResponse>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * Borrar un pago
   * Solo se pueden borrar pagos que estén pendientes de pagar. Esta operación no puede deshacerse.
      * @param id Identificador del pago (required)
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
   
   * End possible optional params list
   *
      * @return SuccessResponse
   * @throws ApiException if fails to make API call
   */
  public SuccessResponse paymentsIdDelete(String id, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling paymentsIdDelete");
    }
    // create path and map variables
    String localVarPath = "/payments/{id}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }


    if(options != null) {
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<SuccessResponse> localVarReturnType = new GenericType<SuccessResponse>() {};
    return apiClient.invokeAPI(localVarPath, "DELETE", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * Obtener información de un pago
   * Información completa del pago. Datos con los que fue creado y el estado actual del pago.
      * @param id Identificador del pago (required)
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
   
   * End possible optional params list
   *
      * @return PaymentsResponse
   * @throws ApiException if fails to make API call
   */
  public PaymentsResponse paymentsIdGet(String id, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling paymentsIdGet");
    }
    // create path and map variables
    String localVarPath = "/payments/{id}".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }


    if(options != null) {
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<PaymentsResponse> localVarReturnType = new GenericType<PaymentsResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * Reembolsar total o parcialmente un pago
   * Reembolsa total o parcialmente el monto de un pago. Esta operación solo se puede realizar en los comercios que recauden en cuenta khipu y antes de la rendición de los fondos correspondientes.
      * @param id Identificador del pago (required)
      
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
      * - amount 
   * End possible optional params list
   *
      * @return SuccessResponse
   * @throws ApiException if fails to make API call
   */
  public SuccessResponse paymentsIdRefundsPost(String id, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'id' is set
    if (id == null) {
      throw new ApiException(400, "Missing the required parameter 'id' when calling paymentsIdRefundsPost");
    }
    // create path and map variables
    String localVarPath = "/payments/{id}/refunds".replaceAll("\\{format\\}","json")
      .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }


    if(options != null) {
        if(options.containsKey("amount") && options.get("amount") != null) {
            localVarFormParams.put("amount", options.get("amount"));
        }
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/x-www-form-urlencoded"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<SuccessResponse> localVarReturnType = new GenericType<SuccessResponse>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }
  /**
   * Crear un pago
   * Crea un pago en khipu y obtiene las URLs para redirección al usuario para que complete el pago.
      * @param subject  (required)
      * @param currency  (required)
      * @param amount  (required)
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):
            * - transactionId    * - custom    * - body    * - bankId    * - returnUrl    * - cancelUrl    * - pictureUrl    * - notifyUrl    * - contractUrl    * - notifyApiVersion    * - expiresDate    * - sendEmail    * - payerName    * - payerEmail    * - sendReminders    * - responsibleUserEmail    * - fixedPayerPersonalIdentifier    * - integratorFee    * - collectAccountUuid    * - confirmTimeoutDate    * - mandatoryPaymentMethod 
   * End possible optional params list
   *
      * @return PaymentsCreateResponse
   * @throws ApiException if fails to make API call
   */
  public PaymentsCreateResponse paymentsPost(String subject, String currency, Double amount, Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // verify the required parameter 'subject' is set
    if (subject == null) {
      throw new ApiException(400, "Missing the required parameter 'subject' when calling paymentsPost");
    }
    // verify the required parameter 'currency' is set
    if (currency == null) {
      throw new ApiException(400, "Missing the required parameter 'currency' when calling paymentsPost");
    }
    // verify the required parameter 'amount' is set
    if (amount == null) {
      throw new ApiException(400, "Missing the required parameter 'amount' when calling paymentsPost");
    }
    // create path and map variables
    String localVarPath = "/payments".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }

    if (subject != null) {
        localVarFormParams.put("subject", subject);
    }
    if (currency != null) {
        localVarFormParams.put("currency", currency);
    }
    if (amount != null) {
        localVarFormParams.put("amount", amount);
    }

    if(options != null) {
        if(options.containsKey("transactionId") && options.get("transactionId") != null) {
            localVarFormParams.put("transaction_id", options.get("transactionId"));
        }
        if(options.containsKey("custom") && options.get("custom") != null) {
            localVarFormParams.put("custom", options.get("custom"));
        }
        if(options.containsKey("body") && options.get("body") != null) {
            localVarFormParams.put("body", options.get("body"));
        }
        if(options.containsKey("bankId") && options.get("bankId") != null) {
            localVarFormParams.put("bank_id", options.get("bankId"));
        }
        if(options.containsKey("returnUrl") && options.get("returnUrl") != null) {
            localVarFormParams.put("return_url", options.get("returnUrl"));
        }
        if(options.containsKey("cancelUrl") && options.get("cancelUrl") != null) {
            localVarFormParams.put("cancel_url", options.get("cancelUrl"));
        }
        if(options.containsKey("pictureUrl") && options.get("pictureUrl") != null) {
            localVarFormParams.put("picture_url", options.get("pictureUrl"));
        }
        if(options.containsKey("notifyUrl") && options.get("notifyUrl") != null) {
            localVarFormParams.put("notify_url", options.get("notifyUrl"));
        }
        if(options.containsKey("contractUrl") && options.get("contractUrl") != null) {
            localVarFormParams.put("contract_url", options.get("contractUrl"));
        }
        if(options.containsKey("notifyApiVersion") && options.get("notifyApiVersion") != null) {
            localVarFormParams.put("notify_api_version", options.get("notifyApiVersion"));
        }
        if(options.containsKey("expiresDate") && options.get("expiresDate") != null) {
            localVarFormParams.put("expires_date", options.get("expiresDate"));
        }
        if(options.containsKey("sendEmail") && options.get("sendEmail") != null) {
            localVarFormParams.put("send_email", options.get("sendEmail"));
        }
        if(options.containsKey("payerName") && options.get("payerName") != null) {
            localVarFormParams.put("payer_name", options.get("payerName"));
        }
        if(options.containsKey("payerEmail") && options.get("payerEmail") != null) {
            localVarFormParams.put("payer_email", options.get("payerEmail"));
        }
        if(options.containsKey("sendReminders") && options.get("sendReminders") != null) {
            localVarFormParams.put("send_reminders", options.get("sendReminders"));
        }
        if(options.containsKey("responsibleUserEmail") && options.get("responsibleUserEmail") != null) {
            localVarFormParams.put("responsible_user_email", options.get("responsibleUserEmail"));
        }
        if(options.containsKey("fixedPayerPersonalIdentifier") && options.get("fixedPayerPersonalIdentifier") != null) {
            localVarFormParams.put("fixed_payer_personal_identifier", options.get("fixedPayerPersonalIdentifier"));
        }
        if(options.containsKey("integratorFee") && options.get("integratorFee") != null) {
            localVarFormParams.put("integrator_fee", options.get("integratorFee"));
        }
        if(options.containsKey("collectAccountUuid") && options.get("collectAccountUuid") != null) {
            localVarFormParams.put("collect_account_uuid", options.get("collectAccountUuid"));
        }
        if(options.containsKey("confirmTimeoutDate") && options.get("confirmTimeoutDate") != null) {
            localVarFormParams.put("confirm_timeout_date", options.get("confirmTimeoutDate"));
        }
        if(options.containsKey("mandatoryPaymentMethod") && options.get("mandatoryPaymentMethod") != null) {
            localVarFormParams.put("mandatory_payment_method", options.get("mandatoryPaymentMethod"));
        }
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      "application/x-www-form-urlencoded"
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<PaymentsCreateResponse> localVarReturnType = new GenericType<PaymentsCreateResponse>() {};
    return apiClient.invokeAPI(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }

  /**
  * Obtener información de un pago
  * Información completa del pago. Datos con los que fue creado y el estado actual del pago. Se obtiene del notification_token que envia khipu cuando el pago es conciliado.
  * @param notificationToken Token de notifiación recibido usando la API de notificaiones 1.3 o superior. (required)
  * @return PaymentsResponse
  * @throws ApiException if fails to make API call
   */
  public PaymentsResponse paymentsGet(String notificationToken) throws ApiException {
    return paymentsGet(notificationToken, null);
  
  }
  /**
  * Confirmar el pago.
  * Al confirmar el pago, este será rendido al día siguiente.
  * @param id Identificador del pago (required)
  * @return SuccessResponse
  * @throws ApiException if fails to make API call
   */
  public SuccessResponse paymentsIdConfirmPost(String id) throws ApiException {
    return paymentsIdConfirmPost(id, null);
  
  }
  /**
  * Borrar un pago
  * Solo se pueden borrar pagos que estén pendientes de pagar. Esta operación no puede deshacerse.
  * @param id Identificador del pago (required)
  * @return SuccessResponse
  * @throws ApiException if fails to make API call
   */
  public SuccessResponse paymentsIdDelete(String id) throws ApiException {
    return paymentsIdDelete(id, null);
  
  }
  /**
  * Obtener información de un pago
  * Información completa del pago. Datos con los que fue creado y el estado actual del pago.
  * @param id Identificador del pago (required)
  * @return PaymentsResponse
  * @throws ApiException if fails to make API call
   */
  public PaymentsResponse paymentsIdGet(String id) throws ApiException {
    return paymentsIdGet(id, null);
  
  }
  /**
  * Reembolsar total o parcialmente un pago
  * Reembolsa total o parcialmente el monto de un pago. Esta operación solo se puede realizar en los comercios que recauden en cuenta khipu y antes de la rendición de los fondos correspondientes.
  * @param id Identificador del pago (required)
  * @return SuccessResponse
  * @throws ApiException if fails to make API call
   */
  public SuccessResponse paymentsIdRefundsPost(String id) throws ApiException {
    return paymentsIdRefundsPost(id, null);
  
  }
  /**
  * Crear un pago
  * Crea un pago en khipu y obtiene las URLs para redirección al usuario para que complete el pago.
  * @param subject  (required)
  * @param currency  (required)
  * @param amount  (required)
  * @return PaymentsCreateResponse
  * @throws ApiException if fails to make API call
   */
  public PaymentsCreateResponse paymentsPost(String subject, String currency, Double amount) throws ApiException {
    return paymentsPost(subject, currency, amount, null);
  
  }
}
