package com.khipu.api.client;

import com.khipu.ApiException;
import com.khipu.ApiClient;
import com.khipu.Configuration;
import com.khipu.Pair;

import javax.ws.rs.core.GenericType;

import com.khipu.api.model.AuthorizationError;
import com.khipu.api.model.BanksResponse;
import com.khipu.api.model.ServiceError;
import com.khipu.api.model.ValidationError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen")public class BanksApi {
  private ApiClient apiClient;

  public BanksApi() {
    this(Configuration.getDefaultApiClient());
  }

  public BanksApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Obtener listado de bancos
   * Obtiene el listado de bancos que pueden usarse para pagar a esta cuenta de cobro.
   * @param options Map containing optional params to the request
   *
   * Possible params in options map (all params in this map are optional):

   * End possible optional params list
   *
      * @return BanksResponse
   * @throws ApiException if fails to make API call
   */
  public BanksResponse banksGet(Map<String, Object> options) throws ApiException {
    Object localVarPostBody = null;
    // create path and map variables
    String localVarPath = "/banks".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> localVarQueryParams = new ArrayList<Pair>();
    Map<String, String> localVarHeaderParams = new HashMap<String, String>();
    Map<String, Object> localVarFormParams = new HashMap<String, Object>();


    if (options != null) {
    }


    if(options != null) {
    }


    if(options != null) {
    }

    final String[] localVarAccepts = {
      "application/json"
    };
    final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);

    final String[] localVarContentTypes = {
      
    };
    final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);

    String[] localVarAuthNames = new String[] { "khipu" };

    GenericType<BanksResponse> localVarReturnType = new GenericType<BanksResponse>() {};
    return apiClient.invokeAPI(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAccept, localVarContentType, localVarAuthNames, localVarReturnType);
  }

  /**
  * Obtener listado de bancos
  * Obtiene el listado de bancos que pueden usarse para pagar a esta cuenta de cobro.
  * @return BanksResponse
  * @throws ApiException if fails to make API call
   */
  public BanksResponse banksGet() throws ApiException {
    return banksGet(null);
  
  }
}
