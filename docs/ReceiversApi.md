# ReceiversApi
A java wrapper for the khipu APIs

All URIs are relative to *https://khipu.com/api/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**receiversPost**](ReceiversApi.md#receiversPost) | **POST** /receivers | Crear una nueva cuenta de cobro

<a name="receiversPost"></a>
# **receiversPost**
> ReceiversCreateResponse receiversPost(adminFirstName, adminLastName, adminEmail, countryCode, businessIdentifier, businessCategory, businessName, businessPhone, businessAddressLine1, businessAddressLine2, businessAddressLine3, contactFullName, contactJobTitle, contactEmail, contactPhone, bankAccountBankId, bankAccountIdentifier, bankAccountName, bankAccountNumber, notifyUrl, renditionUrl)

Crear una nueva cuenta de cobro

Crear una nueva cuenta de cobro asociada a un integrador. Necesita datos de la cuenta de usuario asociada, datos de facturación y datos de contacto.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.ReceiversApi;


ReceiversApi apiInstance = new ReceiversApi();
String adminFirstName = "adminFirstName_example"; // String | 
String adminLastName = "adminLastName_example"; // String | 
String adminEmail = "adminEmail_example"; // String | 
String countryCode = "countryCode_example"; // String | 
String businessIdentifier = "businessIdentifier_example"; // String | 
String businessCategory = "businessCategory_example"; // String | 
String businessName = "businessName_example"; // String | 
String businessPhone = "businessPhone_example"; // String | 
String businessAddressLine1 = "businessAddressLine1_example"; // String | 
String businessAddressLine2 = "businessAddressLine2_example"; // String | 
String businessAddressLine3 = "businessAddressLine3_example"; // String | 
String contactFullName = "contactFullName_example"; // String | 
String contactJobTitle = "contactJobTitle_example"; // String | 
String contactEmail = "contactEmail_example"; // String | 
String contactPhone = "contactPhone_example"; // String | 
String bankAccountBankId = "bankAccountBankId_example"; // String | 
String bankAccountIdentifier = "bankAccountIdentifier_example"; // String | 
String bankAccountName = "bankAccountName_example"; // String | 
String bankAccountNumber = "bankAccountNumber_example"; // String | 
String notifyUrl = "notifyUrl_example"; // String | 
String renditionUrl = "renditionUrl_example"; // String | 
try {
    ReceiversCreateResponse result = apiInstance.receiversPost(adminFirstName, adminLastName, adminEmail, countryCode, businessIdentifier, businessCategory, businessName, businessPhone, businessAddressLine1, businessAddressLine2, businessAddressLine3, contactFullName, contactJobTitle, contactEmail, contactPhone, bankAccountBankId, bankAccountIdentifier, bankAccountName, bankAccountNumber, notifyUrl, renditionUrl);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReceiversApi#receiversPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adminFirstName** | **String**|  |
 **adminLastName** | **String**|  |
 **adminEmail** | **String**|  |
 **countryCode** | **String**|  |
 **businessIdentifier** | **String**|  |
 **businessCategory** | **String**|  |
 **businessName** | **String**|  |
 **businessPhone** | **String**|  |
 **businessAddressLine1** | **String**|  |
 **businessAddressLine2** | **String**|  |
 **businessAddressLine3** | **String**|  |
 **contactFullName** | **String**|  |
 **contactJobTitle** | **String**|  |
 **contactEmail** | **String**|  |
 **contactPhone** | **String**|  |
 **bankAccountBankId** | **String**|  | [optional]
 **bankAccountIdentifier** | **String**|  | [optional]
 **bankAccountName** | **String**|  | [optional]
 **bankAccountNumber** | **String**|  | [optional]
 **notifyUrl** | **String**|  | [optional]
 **renditionUrl** | **String**|  | [optional]

### Return type

[**ReceiversCreateResponse**](ReceiversCreateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

