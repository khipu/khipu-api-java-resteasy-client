# PaymentsCreateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentId** | **String** | Identificador único del pago, es una cadena alfanumérica de 12 caracteres |  [optional]
**paymentUrl** | **String** | URL principal del pago, si el usuario no ha elegido previamente un método de pago se le muestran las opciones |  [optional]
**simplifiedTransferUrl** | **String** | URL de pago simplificado |  [optional]
**transferUrl** | **String** | URL de pago normal |  [optional]
**webpayUrl** | **String** | URL de pago usando Webpay |  [optional]
**hitesUrl** | **String** | URL de pago usando Hites |  [optional]
**paymeUrl** | **String** | URL de pago usando Hites |  [optional]
**appUrl** | **String** | URL para invocar el pago desde un dispositivo móvil usando la APP de khipu |  [optional]
**readyForTerminal** | **Boolean** | Es &#x27;true&#x27; si el pago ya cuenta con todos los datos necesarios para abrir directamente la aplicación de pagos khipu |  [optional]
