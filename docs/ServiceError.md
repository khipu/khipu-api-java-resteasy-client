# ServiceError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Integer** | Código del error |  [optional]
**message** | **String** | Mensaje del error |  [optional]
