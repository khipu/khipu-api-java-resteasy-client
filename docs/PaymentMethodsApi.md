# PaymentMethodsApi
A java wrapper for the khipu APIs

All URIs are relative to *https://khipu.com/api/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**merchantsIdPaymentMethodsGet**](PaymentMethodsApi.md#merchantsIdPaymentMethodsGet) | **GET** /merchants/{id}/paymentMethods | Obtener listado de medios de pago disponible para una cuenta de cobrador

<a name="merchantsIdPaymentMethodsGet"></a>
# **merchantsIdPaymentMethodsGet**
> PaymentMethodsResponse merchantsIdPaymentMethodsGet(id)

Obtener listado de medios de pago disponible para una cuenta de cobrador

Obtiene el listado de medios de pago que pueden usarse para pagar a esta cuenta de cobro.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.PaymentMethodsApi;


PaymentMethodsApi apiInstance = new PaymentMethodsApi();
String id = "id_example"; // String | Identificador del merchant
try {
    PaymentMethodsResponse result = apiInstance.merchantsIdPaymentMethodsGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentMethodsApi#merchantsIdPaymentMethodsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Identificador del merchant |

### Return type

[**PaymentMethodsResponse**](PaymentMethodsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

