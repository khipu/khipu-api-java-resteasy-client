# PaymentMethodItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Identificador del medio de pago |  [optional]
**name** | **String** | Nombre del medio de pago |  [optional]
**logoUrl** | **String** | URL del logo sugerido para mostrar |  [optional]
