# SuccessResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** | Mensaje a desplegar al usuario |  [optional]
