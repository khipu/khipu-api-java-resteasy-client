# PaymentsApi
A java wrapper for the khipu APIs

All URIs are relative to *https://khipu.com/api/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentsGet**](PaymentsApi.md#paymentsGet) | **GET** /payments | Obtener información de un pago
[**paymentsIdConfirmPost**](PaymentsApi.md#paymentsIdConfirmPost) | **POST** /payments/{id}/confirm | Confirmar el pago.
[**paymentsIdDelete**](PaymentsApi.md#paymentsIdDelete) | **DELETE** /payments/{id} | Borrar un pago
[**paymentsIdGet**](PaymentsApi.md#paymentsIdGet) | **GET** /payments/{id} | Obtener información de un pago
[**paymentsIdRefundsPost**](PaymentsApi.md#paymentsIdRefundsPost) | **POST** /payments/{id}/refunds | Reembolsar total o parcialmente un pago
[**paymentsPost**](PaymentsApi.md#paymentsPost) | **POST** /payments | Crear un pago

<a name="paymentsGet"></a>
# **paymentsGet**
> PaymentsResponse paymentsGet(notificationToken)

Obtener información de un pago

Información completa del pago. Datos con los que fue creado y el estado actual del pago. Se obtiene del notification_token que envia khipu cuando el pago es conciliado.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.PaymentsApi;


PaymentsApi apiInstance = new PaymentsApi();
String notificationToken = "notificationToken_example"; // String | Token de notifiación recibido usando la API de notificaiones 1.3 o superior.
try {
    PaymentsResponse result = apiInstance.paymentsGet(notificationToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentsApi#paymentsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notificationToken** | **String**| Token de notifiación recibido usando la API de notificaiones 1.3 o superior. |

### Return type

[**PaymentsResponse**](PaymentsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentsIdConfirmPost"></a>
# **paymentsIdConfirmPost**
> SuccessResponse paymentsIdConfirmPost(id)

Confirmar el pago.

Al confirmar el pago, este será rendido al día siguiente.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.PaymentsApi;


PaymentsApi apiInstance = new PaymentsApi();
String id = "id_example"; // String | Identificador del pago
try {
    SuccessResponse result = apiInstance.paymentsIdConfirmPost(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentsApi#paymentsIdConfirmPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Identificador del pago |

### Return type

[**SuccessResponse**](SuccessResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentsIdDelete"></a>
# **paymentsIdDelete**
> SuccessResponse paymentsIdDelete(id)

Borrar un pago

Solo se pueden borrar pagos que estén pendientes de pagar. Esta operación no puede deshacerse.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.PaymentsApi;


PaymentsApi apiInstance = new PaymentsApi();
String id = "id_example"; // String | Identificador del pago
try {
    SuccessResponse result = apiInstance.paymentsIdDelete(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentsApi#paymentsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Identificador del pago |

### Return type

[**SuccessResponse**](SuccessResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentsIdGet"></a>
# **paymentsIdGet**
> PaymentsResponse paymentsIdGet(id)

Obtener información de un pago

Información completa del pago. Datos con los que fue creado y el estado actual del pago.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.PaymentsApi;


PaymentsApi apiInstance = new PaymentsApi();
String id = "id_example"; // String | Identificador del pago
try {
    PaymentsResponse result = apiInstance.paymentsIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentsApi#paymentsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Identificador del pago |

### Return type

[**PaymentsResponse**](PaymentsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="paymentsIdRefundsPost"></a>
# **paymentsIdRefundsPost**
> SuccessResponse paymentsIdRefundsPost(id, amount)

Reembolsar total o parcialmente un pago

Reembolsa total o parcialmente el monto de un pago. Esta operación solo se puede realizar en los comercios que recauden en cuenta khipu y antes de la rendición de los fondos correspondientes.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.PaymentsApi;


PaymentsApi apiInstance = new PaymentsApi();
String id = "id_example"; // String | Identificador del pago
Double amount = 3.4D; // Double | 
try {
    SuccessResponse result = apiInstance.paymentsIdRefundsPost(id, amount);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentsApi#paymentsIdRefundsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Identificador del pago |
 **amount** | **Double**|  | [optional]

### Return type

[**SuccessResponse**](SuccessResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="paymentsPost"></a>
# **paymentsPost**
> PaymentsCreateResponse paymentsPost(subject, currency, amount, transactionId, custom, body, bankId, returnUrl, cancelUrl, pictureUrl, notifyUrl, contractUrl, notifyApiVersion, expiresDate, sendEmail, payerName, payerEmail, sendReminders, responsibleUserEmail, fixedPayerPersonalIdentifier, integratorFee, collectAccountUuid, confirmTimeoutDate, mandatoryPaymentMethod)

Crear un pago

Crea un pago en khipu y obtiene las URLs para redirección al usuario para que complete el pago.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.PaymentsApi;


PaymentsApi apiInstance = new PaymentsApi();
String subject = "subject_example"; // String | 
String currency = "currency_example"; // String | 
Double amount = 3.4D; // Double | 
String transactionId = "transactionId_example"; // String | 
String custom = "custom_example"; // String | 
String body = "body_example"; // String | 
String bankId = "bankId_example"; // String | 
String returnUrl = "returnUrl_example"; // String | 
String cancelUrl = "cancelUrl_example"; // String | 
String pictureUrl = "pictureUrl_example"; // String | 
String notifyUrl = "notifyUrl_example"; // String | 
String contractUrl = "contractUrl_example"; // String | 
String notifyApiVersion = "notifyApiVersion_example"; // String | 
Date expiresDate = new Date(); // Date | 
Boolean sendEmail = true; // Boolean | 
String payerName = "payerName_example"; // String | 
String payerEmail = "payerEmail_example"; // String | 
Boolean sendReminders = true; // Boolean | 
String responsibleUserEmail = "responsibleUserEmail_example"; // String | 
String fixedPayerPersonalIdentifier = "fixedPayerPersonalIdentifier_example"; // String | 
Double integratorFee = 3.4D; // Double | 
Boolean collectAccountUuid = true; // Boolean | 
String confirmTimeoutDate = "confirmTimeoutDate_example"; // String | 
String mandatoryPaymentMethod = "mandatoryPaymentMethod_example"; // String | 
try {
    PaymentsCreateResponse result = apiInstance.paymentsPost(subject, currency, amount, transactionId, custom, body, bankId, returnUrl, cancelUrl, pictureUrl, notifyUrl, contractUrl, notifyApiVersion, expiresDate, sendEmail, payerName, payerEmail, sendReminders, responsibleUserEmail, fixedPayerPersonalIdentifier, integratorFee, collectAccountUuid, confirmTimeoutDate, mandatoryPaymentMethod);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PaymentsApi#paymentsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subject** | **String**|  |
 **currency** | **String**|  |
 **amount** | **Double**|  |
 **transactionId** | **String**|  | [optional]
 **custom** | **String**|  | [optional]
 **body** | **String**|  | [optional]
 **bankId** | **String**|  | [optional]
 **returnUrl** | **String**|  | [optional]
 **cancelUrl** | **String**|  | [optional]
 **pictureUrl** | **String**|  | [optional]
 **notifyUrl** | **String**|  | [optional]
 **contractUrl** | **String**|  | [optional]
 **notifyApiVersion** | **String**|  | [optional]
 **expiresDate** | **Date**|  | [optional]
 **sendEmail** | **Boolean**|  | [optional]
 **payerName** | **String**|  | [optional]
 **payerEmail** | **String**|  | [optional]
 **sendReminders** | **Boolean**|  | [optional]
 **responsibleUserEmail** | **String**|  | [optional]
 **fixedPayerPersonalIdentifier** | **String**|  | [optional]
 **integratorFee** | **Double**|  | [optional]
 **collectAccountUuid** | **Boolean**|  | [optional]
 **confirmTimeoutDate** | **String**|  | [optional]
 **mandatoryPaymentMethod** | **String**|  | [optional]

### Return type

[**PaymentsCreateResponse**](PaymentsCreateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

