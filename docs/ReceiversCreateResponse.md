# ReceiversCreateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**receiverId** | **String** | Identificador único de la cuenta de cobro |  [optional]
**secret** | **String** | Llave secreta de la cuenta de cobro, se usa para firmar todas las peticiones |  [optional]
