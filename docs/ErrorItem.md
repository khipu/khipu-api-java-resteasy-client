# ErrorItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field** | **String** | Campo que tiene el error de validación |  [optional]
**message** | **String** | Motivo del error de validación |  [optional]
