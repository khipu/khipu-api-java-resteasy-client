# BankItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bankId** | **String** | Identificador del banco |  [optional]
**name** | **String** | Nombre del banco |  [optional]
**message** | **String** | Mensaje con particularidades del banco |  [optional]
**minAmount** | **Double** | Monto mínimo que acepta el banco en un pago |  [optional]
**type** | **String** | Tipo de banco |  [optional]
**parent** | **String** | Identificador del banco padre (si un banco tiene banca personas y empresas, el primero será el padre del segundo) |  [optional]
