# BanksResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**banks** | [**List&lt;BankItem&gt;**](BankItem.md) |  |  [optional]
