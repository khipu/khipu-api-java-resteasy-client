# BanksApi
A java wrapper for the khipu APIs

All URIs are relative to *https://khipu.com/api/2.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**banksGet**](BanksApi.md#banksGet) | **GET** /banks | Obtener listado de bancos

<a name="banksGet"></a>
# **banksGet**
> BanksResponse banksGet()

Obtener listado de bancos

Obtiene el listado de bancos que pueden usarse para pagar a esta cuenta de cobro.

### Example
```java
// Import classes:
//import com.khipu.ApiException;
//import com.khipu.api.client.BanksApi;


BanksApi apiInstance = new BanksApi();
try {
    BanksResponse result = apiInstance.banksGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BanksApi#banksGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BanksResponse**](BanksResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

