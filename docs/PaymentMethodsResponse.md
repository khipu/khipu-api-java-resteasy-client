# PaymentMethodsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**paymentMethods** | [**List&lt;PaymentMethodItem&gt;**](PaymentMethodItem.md) |  |  [optional]
